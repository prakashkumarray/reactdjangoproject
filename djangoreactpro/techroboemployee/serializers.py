from rest_framework import serializers
from .models import Employee


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = ('pk','first_name', 'last_name', 'email', 'phone','address','description')
        