from django.shortcuts import render

from django.http import HttpResponse
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from .models import Employee
from .serializers import EmployeeSerializer





@csrf_exempt
def employee_list(request):
    if request.method == 'GET':
        employee = Employee.objects.all()
        employee_serializer = EmployeeSerializer(employee, many=True)
        msg={'msg':'data retrive successfully'}
        return JsonResponse(msg, safe=False)


