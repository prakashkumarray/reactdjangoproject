from django.apps import AppConfig


class TechroboemployeeConfig(AppConfig):
    name = 'techroboemployee'
