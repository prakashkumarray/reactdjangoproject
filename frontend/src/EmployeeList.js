import  React, { Component } from  'react';
import  EmployeeService  from  './EmployeeService';

const  employeeService  =  new  EmployeeService();

class  EmployeeList extends  Component {

constructor(props) {
    super(props);
    this.state  = {
        Employees: [],
        
    };
    
}
render() {

    return (
        <div  className="Employeelist">
            <table  className="table">
            <thead  key="thead">
            <tr>
                <th>id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {this.state.Employees.map( c  =>
                <tr  key={c.pk}>
                <td>{c.pk}  </td>
                <td>{c.first_name}</td>
                <td>{c.last_name}</td>
                <td>{c.phone}</td>
                <td>{c.email}</td>
                <td>{c.address}</td>
                <td>{c.description}</td>
                <td>
                <button  onClick={(e)=>  this.handleDelete(e,c.pk) }> Delete</button>
                <a  href={"#" + c.pk}> Update</a>
                </td>
            </tr>)}
            </tbody>
            </table>
            
        </div>
        );
  }
}
export  default EmployeeList;