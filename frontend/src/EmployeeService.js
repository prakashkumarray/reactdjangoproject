import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class EmployeeService{

    constructor(){}


    getEmployee() {
        const url = `${API_URL}/employee/list/`;
        return axios.get(url).then(response => response.data);
    }

}